package com.yevster.microservice;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class Microservice {

	private static final String RESPONSE = "Hello V1";
	private static final int LISTENER_PORT = 8080;

	public static void main(final String[] args) {
		Undertow server = Undertow.builder().addHttpListener(LISTENER_PORT, "localhost").setHandler(new HttpHandler() {
			@Override
			public void handleRequest(final HttpServerExchange exchange) throws Exception {
				exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
				exchange.getResponseSender().send(RESPONSE);
			}
		}).build();
		server.start();
	}

}
